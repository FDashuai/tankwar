import java.awt.*;

public class Blood {

	private int x, y, w, h;
	
	private static int step = 0;
	private static int s = 0;
	
	private int[][] pro = {
			{750, 550}, {50, 550}, {50, 50}, {750, 50}, {390, 290}
	};
	
	private boolean live = true;
	
	private GameWindow tc;
	
	public Blood(GameWindow tc) {
		x = pro[0][0];
		y = pro[0][1];
		w = h = 15;
		this.tc = tc;
	}
	
	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public void draw(Graphics g){
		if(!live)
			return;
		if(s == 300){
			s = 0;
			step++;
			if(step == pro.length){
				step = 0;
			}
		}
		Color c = g.getColor();
		g.setColor(Color.MAGENTA);
		x = pro[step][0];
		y = pro[step][1];
		g.fillRect(x, y, w, h);
		g.setColor(c);
		
		s++;
	}
	
	public Rectangle getRect(){
		return new Rectangle(x, y, w, h);
	}
}