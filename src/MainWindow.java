import java.awt.EventQueue;

import javax.activation.ActivationDataFlavor;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Label;
import java.awt.TextField;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.Button;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneLayout;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow {

	public static JFrame frmRunandsurvive;
	public static MainWindow window;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new MainWindow();
					window.frmRunandsurvive.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public static String IPAdr;
	public static String NAME;
	public Vector vip = new Vector();
	
	TextField textField;
	Thread t;
	JTable table;
	DefaultTableModel model;
	Vector vName;
	String[] sss = {"0"};
	public static String connectIP;
	public static String hostName;
	private void initialize() {
		t = new Thread(new FindRoom());
		t.start();
		InetAddress addr;
		try {
			addr = InetAddress.getLocalHost();
			IPAdr = addr.getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frmRunandsurvive = new JFrame();
		frmRunandsurvive.setTitle("TankWar");
		frmRunandsurvive.setBounds(100, 100, 751, 491);
		frmRunandsurvive.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRunandsurvive.getContentPane().setLayout(null);
		frmRunandsurvive.setLocationRelativeTo(null);
		frmRunandsurvive.setResizable(false);
		
		Label label = new Label("Name:");
		label.setBounds(10, 31, 69, 23);
		frmRunandsurvive.getContentPane().add(label);
		
		textField = new TextField();
		textField.setBounds(10, 60, 96, 23);
		frmRunandsurvive.getContentPane().add(textField);
		
		Label label_1 = new Label("\u672C\u673AIP\uFF1A");
		label_1.setBounds(580, 419, 49, 23);
		frmRunandsurvive.getContentPane().add(label_1);
		
		Label label_2 = new Label(IPAdr);
		label_2.setBounds(635, 419, 90, 23);
		frmRunandsurvive.getContentPane().add(label_2);
		
		Button button = new Button("\u52A0\u5165\u623F\u95F4");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if(!textField.getText().equals("")){
					if(table.getSelectedRow()!=-1){
						if(textField.getText().indexOf("@")==-1){
							NAME = textField.getText();
							connectIP = String.valueOf(table.getValueAt(table.getSelectedRow(), 0));
							GameWindow.ServerIP = connectIP;
							hostName = String.valueOf(table.getValueAt(table.getSelectedRow(), 1));
							if(table.getValueAt(table.getSelectedRow(), 0)==null){
								JOptionPane.showMessageDialog(null, "请正确选择房间！", "提示", JOptionPane.ERROR_MESSAGE);
							}else{
								sss[0] = "1";
								Room.main(sss);
							}
						}else{
							JOptionPane.showMessageDialog(null, "输入的名字中不允许有@！", "错误", JOptionPane.ERROR_MESSAGE);
						}
					}else{
						JOptionPane.showMessageDialog(null, "请选择房间！", "提示", JOptionPane.ERROR_MESSAGE);
					}
				}else{
					JOptionPane.showMessageDialog(null, "请输入名字！", "提示", JOptionPane.ERROR_MESSAGE);
				}
				
				//System.out.println(table.getValueAt(table.getSelectedRow(), 0));
			}
		});
		button.setBounds(601, 31, 104, 23);
		frmRunandsurvive.getContentPane().add(button);
		
		Vector vData = new Vector();
		vName = new Vector();
		vName.add("IP");
		vName.add("房主");
		vName.add("人数");
		//Vector vRow = new Vector();
		//vRow.add("cell 0 0");
		//vRow.add("cell 0 1");
		//vRow.add("cell 0 2");
		//vData.add(vRow.clone());
		//vData.add(vRow.clone());
		model = new DefaultTableModel(vData, vName);
		
		/*
			Vector vRow1 = new Vector();
			vRow1.add("cell 2 0");
			vRow1.add("cell 2 1");
			vData.add(vRow1);
			model = new DefaultTableModel(vData, vName);
			jTable1.setModel(model);
		 */
         
        //根据 TableModel来创建 Table
        table = new JTable(model){
        	public boolean isCellEditable(int row, int column) { return false;}//表格不允许被编辑 
        };
        table.setModel(model);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(130, 10, 442, 432);
		scrollPane.setLayout(new ScrollPaneLayout());
		frmRunandsurvive.getContentPane().add(scrollPane);

		Button button_1 = new Button("\u521B\u5EFA\u623F\u95F4");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if(!textField.getText().equals("")){
					if(textField.getText().indexOf("@")==-1){
						//frmRunandsurvive.setVisible(false);												//	调试用
						NAME = textField.getText();
						Room.playerNum = 1;
						sss[0] = "0";
						Room.main(sss);
					}else{
						JOptionPane.showMessageDialog(null, "输入的名字中不允许有@！", "错误", JOptionPane.ERROR_MESSAGE);
					}
				}else{
					JOptionPane.showMessageDialog(null, "请输入名字！", "提示", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		button_1.setBounds(601, 72, 104, 23);
		frmRunandsurvive.getContentPane().add(button_1);

	}
	
	
	String data;
	DatagramSocket ds;
	String[] datas;
	ArrayList<String> al = new ArrayList<String>();
	Vector<Vector> vData = new Vector<Vector>();
	int r = 0;
	int r1 = 0;
	//udp寻找房间
	class FindRoom implements Runnable {
		public void run() {
			try {
				ds = new DatagramSocket(8086);		//接收端口
				while(true){
			        byte[] buf = new byte[1024];//接受内容的大小，注意不要溢出
			        DatagramPacket dp = new DatagramPacket(buf,0,buf.length);//定义一个接收的包
			        ds.receive(dp);//将接受内容封装到包中
			        data = new String(dp.getData(), 0, dp.getLength());//利用getData()方法取出内容
			        
			        datas = data.split("@");
			        //for(int i = 0; i<datas.length; i++){
			        	//System.out.println(datas[i]);
			        //}
			        //System.out.println(datas[0] + " ha " + datas[2]);
			        Vector<String> vRow1 = new Vector<String>();
			        //判断是否已经包含ip
			        Object[] aData = null;
			        if(datas[2].equals("0/4")){
			        	aData = vData.toArray();
			        	//System.out.println(aData[0].toString().split(",")[0]);
			        	Object tgt = new Object();
			        	for(int i=0;i<aData.length;i++){
			        		if(aData[i].toString().split(",")[0].equals("[" + datas[0])){
			        			tgt = aData[i];
			        			vData.remove(tgt);
			        		}
			        	}
			        }else{
			        	if(!al.contains(datas[0])){
				        	al.add(datas[0]);
				        }else{
				        	aData = vData.toArray();
				        	//System.out.println(aData[0].toString().split(",")[0]);
				        	Object tgt = new Object();
				        	for(int i=0;i<aData.length;i++){
				        		if(aData[i].toString().split(",")[0].equals("[" + datas[0])){
				        			tgt = aData[i];
				        			vData.remove(tgt);
				        		}
				        	}
				        }
				        for(int i=0;i<datas.length; i++){
				        	vRow1.add(datas[i]);
				        }
						vData.add(vRow1);
			        }
			        //保持选中行
			        int row = 0;
			        if(table.getSelectedRow()!=-1 && aData!=null && table.getSelectedRow()!=0){
			        	String ip = table.getValueAt(table.getSelectedRow(), 0).toString();
			        	for(int i=0;i<aData.length;i++){
			        		if(aData[i].toString().split(",")[0].equals("[" + ip)){
			        			row = i;
			        		}
			        	}
			        }
					model = new DefaultTableModel(vData, vName);
					table.setModel(model);
					//System.out.println(table.getRowCount());
					if(table.getRowCount()>row && table.getRowCount()!=0){
						table.setRowSelectionInterval(row, row);
					}
			        Thread.sleep(200);
				} 
				//data.codePointCount(0, )
			}catch (SocketException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}finally{
				ds.close();//关闭资源
			}
		}
	}
	
}
