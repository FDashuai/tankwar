import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Tank {
	
	public static final int WIDTH = 30, HEIGHT = 30;
	
	public static final int XSPEED = 4, YSPEED = 4;
	
	private int x = 20,y = 20;			//坦克的位置
	private int oldX, oldY;
	
	private boolean isGood;				//是玩家坦克还是电脑（敌对）坦克
	
	private boolean live = true;		//存活状态
	
	private int life = 100;		//生命值
	
	private Direction dir = Direction.STOP;
	private Direction ptDir = Direction.U;
	
	private GameWindow tc;
	
	private String name = "";
	
	private Color tankColor = Color.red;
	
	private static Random r = new Random();
	
	private int step = r.nextInt(12) + 3;
	
	private BloodBar bb = new BloodBar();
	
	private boolean isControled = false;
	
	//监测键位变化
	private boolean bL = false, bU = false, bR = false, bD = false;
	
	public void setBL(boolean b){
		bL = b;
		locateDirection();
	}
	
	public void setBU(boolean b){
		bU = b;
		locateDirection();
	}
	
	public void setBR(boolean b){
		bR = b;
		locateDirection();
	}
	
	public void setBD(boolean b){
		bD = b;
		locateDirection();
	}
	
	public boolean isLive(){
		return live;
	}
	
	public void setLive(boolean live){
		this.live = live;
	}
	
	public String getName(){
		return name;
	}
	
	public int getLife() {
		return life;
	}
	
	public boolean getLive() {
		return live;
	}

	public void setLife(int life) {
		this.life = life;
	}
	
	public boolean isGood() {
		return isGood;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public Color getColor(){
		return tankColor;
	}
	
	public void setDir(Direction d){
		this.dir = d;
	}
	
	public void setIsControled(boolean ic) {
		isControled = ic;
	}
	
	public Tank(int x, int y, boolean isGood) {
		this.x = x;
		this.y = y;
		this.isGood = isGood;
	}
	
	public Tank(int x, int y, boolean isGood, String name, Color c, Direction dir, GameWindow gw) {
		this(x, y, isGood);
		this.name = name;
		this.dir = dir;
		this.tc = gw;
		this.tankColor = c;
		new Thread(new TankMove()).start();
		new Thread(new TankSite()).start();
		if(isGood && tc.sss[0].equals("0")) {
			new Thread(new TankLf()).start();
			System.out.println(name);
		}
		//System.out.println(name);
	}
	
	public void draw(Graphics g){
		if(!isLive()){
			return;
		}
		Color color = g.getColor();
		if(isGood){
			g.setColor(tankColor);
			bb.draw(g);
		}else{
			g.setColor(Color.GRAY);
		}
		g.fillOval(x, y, WIDTH, HEIGHT);
		g.setColor(color);
		
		switch(ptDir){
		case L:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x, y + Tank.HEIGHT/2);
			break;
		case LU:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x, y);
			break;
		case U:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x + Tank.WIDTH/2, y);
			break;
		case RU:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x + Tank.WIDTH, y);
			break;
		case R:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x + Tank.WIDTH, y + Tank.HEIGHT/2);
			break;
		case RD:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x + Tank.WIDTH, y + Tank.HEIGHT);
			break;
		case D:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x + Tank.WIDTH/2, y + Tank.HEIGHT);
			break;
		case LD:
			g.drawLine(x + Tank.WIDTH/2, y + Tank.HEIGHT/2, x, y + Tank.HEIGHT);
			break;
		}
		
		//move();
		
	}
	
	public void move(){
		oldX = x;
		oldY = y;
		switch(dir){
		case L:
			x = x - XSPEED;
			break;
		case LU:
			x = x - XSPEED;
			y = y - YSPEED;
			break;
		case U:
			y = y - YSPEED;
			break;
		case RU:
			x = x + XSPEED;
			y = y - YSPEED;
			break;
		case R:
			x = x + XSPEED;
			break;
		case RD:
			x = x + XSPEED;
			y = y + YSPEED;
			break;
		case D:
			y = y + YSPEED;
			break;
		case LD:
			x = x - XSPEED;
			y = y + YSPEED;
			break;
		case STOP:
			break;
		}
		if(this.dir != Direction.STOP){
			this.ptDir = this.dir;
		}
		
		//边缘检测
		if(x <= 0) 
			x = 0;
		if(y <= 30)
			y = 30;
		if(x + this.WIDTH > tc.GAME_WIDTH)
			x = tc.GAME_WIDTH - this.HEIGHT;
		if(y + this.HEIGHT > tc.GAME_HEIGHT)
			y = tc.GAME_HEIGHT - this.HEIGHT;
		
		if(tc.sss[0].equals("0")){
			if(!isGood){
				Direction[] dirs = Direction.values();
				if(step == 0){
					step = r.nextInt(12) + 3;
					int rn = r.nextInt(dirs.length);
					dir = dirs[rn];
					tc.sendData(name + "@" + "4" + "@" + String.valueOf(rn));
				}else {
					step--;
				}
				if(r.nextInt(40) > 38){
					this.fire();
					tc.sendData(name + "@" + "6" + "@" + "X");				//敌对坦克开炮
				}
			}
		}
		
	}
	
	
	public void keyPressed(KeyEvent e){
		int key = e.getKeyCode();
		switch(key){
		case KeyEvent.VK_F2 :
			if(!live){
				this.live = true;
				this.life = 100;
			}
			break;
		case KeyEvent.VK_W : 
			bU = true;
			tc.sendData(name + "@" + "0" + "@" + "W");				//数据格式：名称+0（表示按下）/1（表示松开）+按键名称
			break;
		case KeyEvent.VK_A : 
			bL = true;
			tc.sendData(name + "@" + "0" + "@" + "A");
			break;
		case KeyEvent.VK_S : 
			bD = true;
			tc.sendData(name + "@" + "0" + "@" + "S");
			break;
		case KeyEvent.VK_D : 
			bR = true;
			tc.sendData(name + "@" + "0" + "@" + "D");
			break;
		}
		locateDirection();
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		switch(key){
		case KeyEvent.VK_J : 
			fire();
			tc.sendData(name + "@" + "2" + "@" + "X");
			break;
		case KeyEvent.VK_W : 
			bU = false;
			tc.sendData(name + "@" + "1" + "@" + "W");
			break;
		case KeyEvent.VK_A : 
			bL = false;
			tc.sendData(name + "@" + "1" + "@" + "A");
			break;
		case KeyEvent.VK_S : 
			bD = false;
			tc.sendData(name + "@" + "1" + "@" + "S");
			break;
		case KeyEvent.VK_D : 
			bR = false;
			tc.sendData(name + "@" + "1" + "@" + "D");
			break;
		case KeyEvent.VK_K :
			//superFire();
			System.out.println(tc.tanks.size());
			break;
		}
		locateDirection();
	}
	
	public void fire(){
		if(!live){
			return;
		}
		Missile m = new Missile(x + Tank.WIDTH/2 - Missile.WIDTH/2,
				y + Tank.HEIGHT/2 - Missile.HEIGHT/2, this, ptDir, tc);
		tc.missiles.add(m);
	}
	
	public void fire(Direction dirs){
		if(!live){
			return;
		}
		Missile m = new Missile(x + Tank.WIDTH/2 - Missile.WIDTH/2,
				y + Tank.HEIGHT/2 - Missile.HEIGHT/2, this, dirs, tc);
		tc.missiles.add(m);
	}
	
	public void locateDirection(){
		if(bL && !bU && !bR && !bD)
			dir = Direction.L;
		if(bL && bU && !bR && !bD)
			dir = Direction.LU;
		if(!bL && bU && !bR && !bD)
			dir = Direction.U;
		if(!bL && bU && bR && !bD)
			dir = Direction.RU;
		if(!bL && !bU && bR && !bD)
			dir = Direction.R;
		if(!bL && !bU && bR && bD)
			dir = Direction.RD;
		if(!bL && !bU && !bR && bD)
			dir = Direction.D;
		if(bL && !bU && !bR && bD)
			dir = Direction.LD;
		if(!bL && !bU && !bR && !bD)
			dir = Direction.STOP;
		
		//move();
	}
	
	class TankMove implements Runnable{
		@Override
		public void run() {
			while (isLive()) {
				move();
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} 
	}
	
	public Rectangle getRect(){
		return new Rectangle(x, y, this.WIDTH, this.HEIGHT);
	}
	
	public void stay(){
		x = oldX;
		y = oldY;
	}
	
	public boolean collidesWithTanks(java.util.List<Tank> tanks){
		for(int i=0;i<tanks.size();i++){
			Tank t = tanks.get(i);
			if(live && this != t && t.isLive() && this.getRect().intersects(t.getRect())){
				this.stay();
				return true;
			}
		}
		return false;
	}
	
	public boolean eat(Blood b){
		if(live && b.isLive() && this.getRect().intersects(b.getRect())){
			b.setLive(false);
			this.life = 100;
			return true;
		}
		return false;
	}
	
	private class BloodBar {
		public void draw (Graphics g){
			Color c = g.getColor();
			g.setColor(tankColor);
			g.drawRect(x, y-9, WIDTH, 8);
			int w = WIDTH * life/100;
			g.fillRect(x, y-9, w, 8);
			g.setColor(c);
		}
	}
	
	class TankSite implements Runnable{
		@Override
		public void run() {
			while(isLive()) {
				if(isGood) {
					tc.sendData(name + "@" + "3" + "@" + String.valueOf(x) + "@" + String.valueOf(y));
				}else {
					tc.sendData(name + "@" + "5" + "@" + String.valueOf(x) + "@" + String.valueOf(y));
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	class TankLf implements Runnable{

		@Override
		public void run() {
			while(true) {
				tc.sendData(name + "@" + "10" + "@" + String.valueOf(life));
				System.out.println(name);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
