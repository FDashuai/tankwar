import java.awt.*;

public class Explode {

	private int x, y;
	private boolean live = true;
	private int[] diameter = {4, 7, 12, 18, 26, 32, 49, 30, 14, 6};
	private int step = 0;
	private GameWindow tc;
	private Tank t;
	
	public Explode(int x, int y, GameWindow tc, Tank t) {
		this.x = x;
		this.y = y;
		this.tc = tc;
		this.t = t;
	}
	
	public void draw(Graphics g){
		if(!live){
			tc.explodes.remove(this);
			return;
		}
		if(step == diameter.length){
			live = false;
			step = 0;
		}
		Color c = g.getColor();
		g.setColor(Color.ORANGE);
		g.fillOval(t.getX() + Tank.WIDTH/2 - diameter[step]/2, t.getY() + Tank.HEIGHT/2 - diameter[step]/2, diameter[step], diameter[step]);
		step++;
		g.setColor(c);
	}
}
