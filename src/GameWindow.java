import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

public class GameWindow extends Frame {

	private JPanel contentPane;
	
	Label lbl1 = new Label("消灭坦克数：");
	Label lbl2 = new Label("0");
	
	public static final int GAME_WIDTH = 800, GAME_HEIGHT = 600;
	
	public static String[] sss;
	
	private static String[] playerIP = new String[3];
	
	public static int playerNum = 1;
	
	public static String ServerIP = "";
	
	public static String[] playerName = new String[3];
	
	public static int DEADTANK = 0;
	
	Tank[] playerTank = new Tank[4];
	
	Blood b = new Blood(this);
	
	ArrayList<Tank> tanks = new ArrayList<Tank>();
	
	ArrayList<Explode> explodes = new ArrayList<Explode>();
	
	ArrayList<Missile> missiles = new ArrayList<Missile>();
	
	ArrayList<Thread> threads = new ArrayList<Thread>();
	
	//SITE用于判断是几号位的坦克，在room中就已经定下来了    0代表服务器的坦克
	public static int SITE = 0;

	public static void setPlayerNum(int pn){
		playerNum = pn;
	}
	
	public static String[] getPlayerIP() {
		return playerIP;
	}

	public static void setPlayerIP(String[] playerIP) {
		GameWindow.playerIP = playerIP;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		sss = args;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GameWindow frame = new GameWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GameWindow() {
		DEADTANK = 0;
		setTitle("TankWar For Net Ver0.1");
		//setDefaultCloseOperation(Frame.EXIT_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {	
			@Override
			public void windowClosing(WindowEvent e) {
				setVisible(false);
				System.exit(0);
			}
		});
		setBounds(100, 100, GAME_WIDTH, GAME_HEIGHT);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(199, 237, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setVisible(true);
		//setContentPane(contentPane);
		this.setLayout(null);
		lbl1.setBackground(new Color(199, 237, 204));
		lbl1.setBounds(300, 580, 80, 20);
		this.add(lbl1);
		lbl2.setBackground(new Color(199, 237, 204));
		lbl2.setBounds(385, 580, 80, 20);
		this.add(lbl2);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		
		try {
	          ds = new DatagramSocket(8778);  //实例化套间字，指定自己的port  
	        } catch (SocketException e) {  
	            System.out.println("Cannot open port!");  
	            System.exit(1);   
	        }  
		
		setTank();
		
		Thread t1 = new Thread(new PaintThread());
		threads.add(t1);
		this.addKeyListener(new KeyMonitor());
		Thread t2 = new Thread(new ReceiveData());
		threads.add(t2);
		//Thread t3 = new Thread(new TankSites(true,false));
		//threads.add(t3);
		//Thread t4 = new Thread(new TankSites(false,true));
		//threads.add(t4);
		if(sss[0].equals("0")) {
			Thread t3 = new Thread(new DTkNum());
			threads.add(t3);
			Thread t5 = new Thread(new SetBadTank());
			threads.add(t5);
		}
		/*Thread t6 = new Thread(new TankLife());
		threads.add(t6);*/
		Thread t7 = new Thread(new CollideDetect());
		threads.add(t7);
		Thread t8 = new Thread(new DeadDct());
		threads.add(t8);
		Thread t9 = new Thread(new CountGrade());
		threads.add(t9);
		
		/*if(sss[0].equals("0")) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}*/
		
		for(int i=0;i<threads.size();i++){
			Thread tt = threads.get(i);
			tt.start();
		}
	}
	
	//设置玩家坦克与电脑坦克
	public void setTank(){
		
		if(sss[0].equals("0")){
			playerTank[0] = new Tank(200, 500, true, MainWindow.NAME, Color.red, Direction.STOP, this);
			playerTank[0].setIsControled(true);
		}else{
			playerTank[0] = new Tank(200, 500, true, MainWindow.hostName, Color.red, Direction.STOP, this);
		}
		
		for(int i=1;i<playerNum;i++){
			Color c = Color.BLACK;
			switch (i) {
			case 1:
				c = Color.blue;
				break;
			case 2:
				c = Color.yellow;
				break;
			case 3:
				c = Color.pink;
				break;
			default:
				c = Color.gray;
				break;
			}
			//System.out.println(playerName[i-1]);
			playerTank[i] = new Tank(200 + 50*i, 500, true, playerName[i-1], c, Direction.STOP, this);
			if(SITE==i) {
				playerTank[i].setIsControled(true);
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for(int i=0;i<playerNum;i++){
			if(playerTank[i]!=null){
				playerTank[i].draw(g);
			}
		}
		b.draw(g);
		for(int i=0; i<missiles.size();i++){
			Missile m = missiles.get(i);
			m.hitTanks(tanks);
			m.draw(g);
		}
		
		for(int i=0; i<tanks.size();i++){
			Tank t = tanks.get(i);
			t.draw(g);
		}
		
		for(int i=0; i<explodes.size();i++){
			Explode e = explodes.get(i);
			e.draw(g);
		}
	}
	
	//双缓冲减少游戏画面闪烁
	Image offScreenImage = null;
	public void update(Graphics g) {
		if(offScreenImage == null){
			offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
		}
		Graphics gOffScreen = offScreenImage.getGraphics();
		Color c = gOffScreen.getColor();
		gOffScreen.setColor(new Color(199,237,204));
		gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
		gOffScreen.setColor(c);
		paint(gOffScreen);
		g.drawImage(offScreenImage, 0, 0, null);
	}
	
	class PaintThread implements Runnable{
		public void run() {
			while(contentPane.isVisible()){
				repaint();
				
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	class KeyMonitor extends KeyAdapter{
		public void keyReleased(KeyEvent e) {
			playerTank[SITE].keyReleased(e);
		}
		public void keyPressed(KeyEvent e) {
			playerTank[SITE].keyPressed(e);
		}
	}
	
/*	//用于发送数据的线程
	class ActionMSG implements Runnable {

		public void run() {
			while(true){
				
			}
		}
	}*/
	
	//UDP发送数据
	DatagramSocket ds = null;
	public void sendData(String text){
		
	        byte[] buf= text.getBytes();  //数据
	        InetAddress destination = null ;
	        try {
	        	if(sss[0].equals("0")){				//服务端
	        		//System.out.println(playerIP[i]);
	        		for(int i=0;i<playerNum-1;i++){
	        			destination = InetAddress.getByName(playerIP[i]);  //广播
	        			DatagramPacket dp =   
	        	                new DatagramPacket(buf, buf.length, destination , 8788);    
	        	        //打包到DatagramPacket类型中（DatagramSocket的send()方法接受此类，注意8788是接受地址的端口，不同于自己的端口！）  
	        			ds.send(dp);  //发送数据
	        		}
	        	}else{
	        		destination = InetAddress.getByName(ServerIP);
	        		DatagramPacket dp =   
        	                new DatagramPacket(buf, buf.length, destination , 8788);    
        	        //打包到DatagramPacket类型中（DatagramSocket的send()方法接受此类，注意8788是接受地址的端口，不同于自己的端口！）  
        			ds.send(dp);  //发送数据
	        	}
	            
	        } catch (UnknownHostException e) {  
	            System.out.println("Cannot open findhost!");  
	            ds.close(); 
	            System.exit(1);   
	        } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ds.close(); 
			}
	}
	
	//udp接收数据
	class ReceiveData implements Runnable {
		String data;
		DatagramSocket ds;
		String[] datas;
		public void run() {
			try {
				ds = new DatagramSocket(8788);		//接收端口
				while(contentPane.isVisible()){
					
			        byte[] buf = new byte[1024];//接受内容的大小，注意不要溢出
			        DatagramPacket dp = new DatagramPacket(buf,0,buf.length);//定义一个接收的包
			        ds.receive(dp);//将接受内容封装到包中
			        //System.out.println("ha");
			        data = new String(dp.getData(), 0, dp.getLength());//利用getData()方法取出内容
			        
			        datas = data.split("@");
			        
			        //
			        new Thread(new HandleData(datas)).start();
			        if(sss[0].equals("0")) {
			        	new Thread(new BadTankSite(datas)).start();
			        }
			        
			        new Thread(new BadTankFire(datas)).start();
			        new Thread(new BadTankMove(datas)).start();

			        //System.out.println(datas[2]);
			        int ts = -1;
			        for(int i=0;i<playerNum;i++){
			        	if(i!=SITE || datas[1].equals("10")){
			        		if(playerTank[i].getName().equals(datas[0])){
			        			//System.out.println(datas[0]);
				        		ts = i;
				        		break;
				        	}
			        	}
			        }
			        if(ts!=-1){
			        	if(datas[1].equals("0")){				//0按下
				        	switch (datas[2]) {
							case "W":
								playerTank[ts].setBU(true);
								break;
							case "A":
								playerTank[ts].setBL(true);
								break;
							case "S":
								playerTank[ts].setBD(true);
								break;
							case "D":
								playerTank[ts].setBR(true);
								break;
							default:
								break;
							}
				        }else if(datas[1].equals("1")){				//1松开
				        	switch (datas[2]) {
							case "W":
								playerTank[ts].setBU(false);
								break;
							case "A":
								playerTank[ts].setBL(false);
								break;
							case "S":
								playerTank[ts].setBD(false);
								break;
							case "D":
								playerTank[ts].setBR(false);
								break;
							default:
								break;
							}
				        }else if(datas[1].equals("2")){				//2开火
				        	playerTank[ts].fire();
				        }else if(datas[1].equals("3")){				//3确认坐标
				        	playerTank[ts].setX(Integer.parseInt(datas[2]));
				        	playerTank[ts].setY(Integer.parseInt(datas[3]));
				        }else if(datas[1].equals("9")) {
				        	playerTank[ts].setLive(false);
				        }else if(datas[1].equals("10") && !sss[0].equals("0")) {
				        	playerTank[ts].setLife(Integer.parseInt(datas[2]));
				        	if(Integer.parseInt(datas[2])==0){
				        		playerTank[ts].setLive(false);
				        	}
				        }
			        	
			        }
				}
			}catch(Exception e){
				e.printStackTrace();
				ds.close();
			}
		}
	}
	
	class HandleData implements Runnable{
		
		String[] datas;
		
		public HandleData(String[] d) {
			this.datas = d;
		}
		
		@Override
		public void run() {
			//System.out.println(datas[0]);
			//System.out.println(datas[1]);
				if(datas[1].equals("8")) {
					for(int i=0;i<tanks.size();i++) {
						Tank t = tanks.get(i);
						if(t.getName().equals(datas[0])) {
							//t.setLive(false);
							tanks.remove(t);
							//DEADTANK++;
						}
					}
	        		
		        }else if(datas[1].equals("C") && !sss[0].equals("0")) {
		        	System.out.println("hey");
		        	createTank();
		        }else if(datas[1].equals("D") && !sss[0].equals("0")) {
		        	DEADTANK = Integer.parseInt(datas[0]);
		        }
		}
	}
	
	class BadTankMove implements Runnable{
		
		String[] datas;
		
		public BadTankMove(String[] d) {
			this.datas = d;
		}
		
		@Override
		public void run() {
			if(datas[1].equals("4") && sss[0].equals("1")){				//4敌对坦克移动
	        	int st = 0;
	        	Direction[] dirs = Direction.values();
	        	for(int i=0;i<tanks.size();i++){
	    			Tank t = tanks.get(i);
	    			if(t.getName().equals(datas[0])){
	    				st = i;
	    				break;
	    			}
	        	}
	        	tanks.get(st).setDir(dirs[Integer.parseInt(datas[2])]);
	        }
		}
	}
	
	class BadTankSite implements Runnable{
		
		String[] datas;
		
		public BadTankSite(String[] d) {
			this.datas = d;
		}
		
		@Override
		public void run() {
			
			if(datas[1].equals("5") && sss[0].equals("1")){				//5矫正敌对坦克坐标
	        	int st = 0;
	        	//Direction[] dirs = Direction.values();
	        	for(int i=0;i<tanks.size();i++){
	    			Tank t = tanks.get(i);
	    			if(t.getName().equals(datas[0])){
	    				st = i;
	    				break;
	    			}
	        	}
	        	tanks.get(st).setX(Integer.parseInt(datas[2]));
	        	tanks.get(st).setY(Integer.parseInt(datas[3]));
	        }
			
		}
	}
	
	class BadTankFire implements Runnable{
		
		String[] datas;
		
		public BadTankFire(String[] d) {
			this.datas = d;
		}
		
		@Override
		public void run() {
			
			if(datas[1].equals("6") && sss[0].equals("1")){				//6矫正敌对坦克坐标
	        	int st = 0;
	        	//Direction[] dirs = Direction.values();
	        	for(int i=0;i<tanks.size();i++){
	    			Tank t = tanks.get(i);
	    			if(t.getName().equals(datas[0])){
	    				st = i;
	    				break;
	    			}
	        	}
	        	tanks.get(st).fire();
	        }
			
		}
	}
	
	/*class TankSites implements Runnable{

		boolean flag1 = false,flag2 = false;
		
		public TankSites(boolean f1,boolean f2) {
			this.flag1 = f1;
			this.flag2 = f2;
		}
		
		@Override
		public void run() {
			while (flag1) {
				for(int i=0;i<playerNum;i++){
					if(i!=SITE){
						sendData(playerTank[i].getName() + "@" + "3" + "@" + String.valueOf(playerTank[i].getX()) + "@" + String.valueOf(playerTank[i].getY()));		//3为重定向坐标
					}
				}
				
				
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			while (flag2) {
				for(int i=0; i<tanks.size();i++){
					Tank tk = tanks.get(i);
					sendData(tk.getName() + "@" + "5" + "@" + String.valueOf(tk.getX()) + "@" + String.valueOf(tk.getY()));		//3为重定向坐标
				}
				
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}*/
	
	/*class TankLife implements Runnable{

		@Override
		public void run() {
			while(true) {
				for(int i=0;i<playerNum;i++){
					if(i != SITE) {
						sendData(playerTank[i].getName() + "@" + "7" + 
								"@" + String.valueOf(playerTank[i].getLife()) + "@" + sss[0]);		//7为坦克生命值
					}
				}
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}*/
	
	//生成敌对坦克
	int times = 0;		//生成坦克的轮数
	int tankCount = 0;
	public void createTank(){
		if(sss[0].equals("0")) {
			sendData(String.valueOf(times) + "@" + "C");		//C为产生新的坏坦克
		}
		for(int i=0;i<10;i++){
			tanks.add(new Tank(40 + 80*i, 40, false, "BadTank" + String.valueOf(tankCount),Color.gray, Direction.STOP, this));
			tankCount++;
		}
	}
	
	
	//只会在服务器中产生，当产生新的坏坦克时，发送UDP封包告知其他客户端。
	class SetBadTank implements Runnable{

		@Override
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			while(contentPane.isVisible()){
				//System.out.println(tanks.size());
				if(tanks.size() <= 0){
					
					for(int i=0;i<=times;i++){
						createTank();
						//System.out.println("haha");
						try {
							Thread.sleep(4000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					times++;
				}
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
	}
	
	class CollideDetect implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(contentPane.isVisible()) {
				for(int i=0; i<missiles.size();i++){
					Missile m = missiles.get(i);
					m.hitTanks(tanks);
					for(int j=0;j<playerNum;j++){
						m.hitTank(playerTank[j]);
					}
				}
				
				for(int i=0; i<tanks.size();i++){
					Tank t = tanks.get(i);
					t.collidesWithTanks(tanks);
				}
				
				for(int i=0;i<playerNum;i++){
					playerTank[i].eat(b);
					playerTank[i].collidesWithTanks(tanks);
				}
				
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	//检测是否游戏结束
	class DeadDct implements Runnable{

		@Override
		public void run() {
			boolean flag = true;
			while(flag) {
				int f = 0;
				for(int i=0;i<playerNum;i++) {
					if(playerTank[i].getLive()) {
						f++;
					}
				}
				if(f==0) {
					int yesNo = JOptionPane.showConfirmDialog(null, "游戏结束！你们已全部阵亡。\n" + "得分：" + String.valueOf(DEADTANK) + "要重新开始吗？"
					, "Over!", JOptionPane.YES_NO_OPTION);
					if(yesNo==1) {
						System.exit(0);
					}else {
						//System.out.println("111");
						setVisible(false);
						//打开大厅
						//GameWindow.main(sss);
						MainWindow.window.frmRunandsurvive.setVisible(true);
						for(int i=0;i<threads.size();i++){
							Thread tt = threads.get(i);
							tt.interrupt();
						}
						ds.close();
						dispose();
						flag = false;
					}
				}
				
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
	class CountGrade implements Runnable {
		
		public void run() {
			while(contentPane.isVisible()){
				lbl2.setText(String.valueOf(DEADTANK));
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	//死亡坦克数量同步线程
	class DTkNum implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(contentPane.isVisible()) {
				sendData(String.valueOf(DEADTANK)+ "@" + "D");
				
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

}
