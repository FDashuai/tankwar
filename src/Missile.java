import java.awt.*;
import java.util.List;
public class Missile {
	public static final int XSPEED = 9, YSPEED = 9;
	
	public static final int WIDTH = 10, HEIGHT = 10;
	
	private int x, y;
	
	private boolean live = true;
	
	private boolean good;
	
	private GameWindow tc;
	
	private Tank t;						//被子弹撞上的子弹
	private Tank tk;					//生成该子弹的坦克
	
	public boolean isLive() {
		return live;
	}
	
	Direction dir;
	
	public Missile(int x, int y, Tank tk, Direction dir, GameWindow tc) {
		this.x = x;
		this.y = y;
		this.good = tk.isGood();
		this.dir = dir;
		this.tc = tc;
		this.tk = tk;
		new Thread(new MissileMove()).start();
	}
	
	public void draw(Graphics g){
		if(!live){
			tc.missiles.remove(this);
			return;
		}
		Color c = g.getColor();
		if(good){
			//System.out.println(tk.getName());
			g.setColor(tk.getColor());
		}
		else {
			g.setColor(Color.gray);
		}
		g.fillOval(x, y, WIDTH, HEIGHT);
		g.setColor(c);
		//move();
	}
	
	public void move() {
		switch(dir){
		case L:
			x = x - XSPEED;
			break;
		case LU:
			x = x - XSPEED;
			y = y - YSPEED;
			break;
		case U:
			y = y - YSPEED;
			break;
		case RU:
			x = x + XSPEED;
			y = y - YSPEED;
			break;
		case R:
			x = x + XSPEED;
			break;
		case RD:
			x = x + XSPEED;
			y = y + YSPEED;
			break;
		case D:
			y = y + YSPEED;
			break;
		case LD:
			x = x - XSPEED;
			y = y + YSPEED;
			break;
		}
		if(x < -1 * Missile.WIDTH || y < -1 * Missile.HEIGHT ||
				x > GameWindow.GAME_WIDTH || y > GameWindow.GAME_HEIGHT){
			this.live = false;
		}
	}
	
	public Rectangle getRect(){
		return new Rectangle(x, y, this.WIDTH, this.HEIGHT);
	}
	
	public boolean hitTank(Tank t){
		if(this.live && this.getRect().intersects(t.getRect()) && t.isLive() && this.good != t.isGood()){
			if(t.isGood()){
				if(tc.sss[0].equals("0")){
					t.setLife(t.getLife()-20);
				}
				
				if(t.getLife()<=0){
					t.setLive(false);
					tc.sendData(t.getName() + "@" + "9" + "X");	 			//9为消灭好坦克
				}
			}
			else{
				t.setLive(false);
			}
			this.live = false;
			tc.missiles.remove(this);
			Explode e = new Explode(x, y, tc, t);
			tc.explodes.add(e);
			return true;
		}
		return false;
	}
	
	public boolean hitTanks(List<Tank> tanks){
		for(int i=0;i<tanks.size();i++){
			t = tanks.get(i);
			if(hitTank(t)){
				if(tc.sss[0].equals("0")) {
					tc.sendData(t.getName() + "@" + "8");	 				//8为消灭坏坦克
					tc.DEADTANK++;
				}
				tc.tanks.remove(t);
				return true;
			}
		}
		return false;
	}
	
	public boolean hitWall(Wall w){
		if(this.live && this.getRect().intersects(w.getRect())){
			live = false;
			return true;
		}
		return false;
	}
	
	class MissileMove implements Runnable{
		@Override
		public void run() {
			while (live) {
				move();
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} 
	}
}
