import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Label;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.awt.Checkbox;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Room {
	
	public static int playerNum = 1;
	public static String[] playerName = {"等待加入","等待加入","等待加入"};
	public static int[] STATE = {0,0,0};				//0代表未准备，1代表准备，仅仅服务端才会使用该变量。
	public static String ZT = "0";						//仅仅客户端才会使用的状态变量，用于记录客户端的状态，0为未准备，1为准备。
	public static int SITE;								//仅仅客户端才会使用的状态变量，用于记录客户端用户在房间中的位置。
	public static String[] playerIP = new String[3];
	private static String START = "0";
	
	private JFrame frmRoom;

	/**
	 * Launch the application.
	 */
	static String[] sss;
	public static void main(String[] args) {
		sss = args;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Room window = new Room();
					window.frmRoom.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Room() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	Label label_1;
	Label label_6;
	ClientWriting playerwrt;
	ClientReading playerrt;
	Thread sendRoom;
	Thread t2;
	Checkbox[] checkboxs = new Checkbox[3];
	Label[] labels = new Label[3];
	JButton button;
	JButton button_1;
	private void initialize() {
		frmRoom = new JFrame();
		frmRoom.setTitle("Room");
		frmRoom.setBounds(100, 100, 470, 403);
		frmRoom.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRoom.getContentPane().setLayout(null);
		frmRoom.setLocationRelativeTo(null);
		frmRoom.setResizable(false);
		
		Label label = new Label("\u623F\u4E3B\uFF1A");
		label.setBounds(10, 20, 37, 23);
		frmRoom.getContentPane().add(label);
		
		label_1 = new Label("");
		label_1.setBounds(48, 20, 69, 23);
		frmRoom.getContentPane().add(label_1);
		
		Label label_2 = new Label("\u961F\u5458\uFF1A");
		label_2.setBounds(10, 49, 37, 23);
		frmRoom.getContentPane().add(label_2);
		
		labels[0] = new Label("\u7B49\u5F85\u52A0\u5165");
		labels[0].setBounds(48, 49, 69, 23);
		frmRoom.getContentPane().add(labels[0]);
		
		labels[1] = new Label("\u7B49\u5F85\u52A0\u5165");
		labels[1].setBounds(48, 78, 69, 23);
		frmRoom.getContentPane().add(labels[1]);
		
		labels[2] = new Label("\u7B49\u5F85\u52A0\u5165");
		labels[2].setBounds(48, 107, 69, 23);
		frmRoom.getContentPane().add(labels[2]);
		
		checkboxs[0] = new Checkbox("\u672A\u51C6\u5907");
		checkboxs[0].addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				//切换准备与未准备，并且改变状态变量
				checkboxs[0].setState(!checkboxs[0].getState());
				if(checkboxs[0].getState()){
					checkboxs[0].setLabel("准备");
					ZT = "1";
				}else{
					checkboxs[0].setLabel("未准备");
					ZT = "0";
				}
			}
		});
		checkboxs[0].setEnabled(false);
		checkboxs[0].setBounds(123, 49, 55, 23);
		frmRoom.getContentPane().add(checkboxs[0]);
		
		checkboxs[1] = new Checkbox("\u672A\u51C6\u5907");
		checkboxs[1].addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				//切换准备与未准备，并且改变状态变量
				checkboxs[1].setState(!checkboxs[1].getState());
				if(checkboxs[1].getState()){
					checkboxs[1].setLabel("准备");
					ZT = "1";
				}else{
					checkboxs[1].setLabel("未准备");
					ZT = "0";
				}
			}
		});
		checkboxs[1].setEnabled(false);
		checkboxs[1].setBounds(123, 78, 55, 23);
		frmRoom.getContentPane().add(checkboxs[1]);
		
		checkboxs[2] = new Checkbox("\u672A\u51C6\u5907");
		checkboxs[2].addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				//切换准备与未准备，并且改变状态变量
				checkboxs[2].setState(!checkboxs[2].getState());
				if(checkboxs[2].getState()){
					checkboxs[2].setLabel("准备");
					ZT = "1";
				}else{
					checkboxs[2].setLabel("未准备");
					ZT = "0";
				}
			}
		});
		checkboxs[2].setEnabled(false);
		checkboxs[2].setBounds(123, 107, 55, 23);
		frmRoom.getContentPane().add(checkboxs[2]);
		
		button = new JButton("\u5F00\u59CB\u6E38\u620F");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//载入游戏
				//首先设置好开始游戏的变量，0为未开始，1为开始
				//System.out.println("he");
				flag = false;
				START = "1";
				frmRoom.setVisible(false);
				GameWindow.setPlayerIP(playerIP);
				GameWindow.setPlayerNum(playerNum);
				GameWindow.playerName = playerName;
				GameWindow.main(sss);
				new Thread(new CloseRoom()).start();
			}
		});
		button.setEnabled(false);
		button.setBounds(318, 20, 88, 39);
		frmRoom.getContentPane().add(button);
		button_1 = new JButton("\u9000\u51FA\u623F\u95F4");
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//退出房间
				//MainWindow.frmRunandsurvive.setVisible(true);
				if(sss[0].equals("1")){
					try {
						playerwrt.socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(sss[0].equals("0")){
					//中断前应该发送删除房间的消息
					frmRoom.setVisible(false);
					playerNum = 0;
					try {
						Thread.sleep(2000);
						ds.close();
						Socket socket = new Socket("localhost",8686);
						ss.close();
						socket.close();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					t2.interrupt();
					sendRoom.interrupt();
				}
				frmRoom.dispose();
			}
		});
		button_1.setBounds(318, 78, 88, 39);
		frmRoom.getContentPane().add(button_1);
		
		//sss为从mainwindow传送过来的参数，0代表创建房间，作为服务器；1代表加入房间，作为客户端。
		if(sss!=null){
			if(sss[0].equals("0")){								//	即服务端
				sendRoom = new Thread(new SendRoom());
				sendRoom.start();
				label_1.setText(MainWindow.NAME);
				//建立TCP服务器
				t2 = new Thread(new createTCP());
				t2.start();
				//建立按钮监听线程
				new Thread(new GameStart()).start();
			}else{
				Socket s;
				try {
					s = new Socket(MainWindow.connectIP,8686);
					playerwrt = new ClientWriting(s);
					Thread pwrt = new Thread(playerwrt);
					pwrt.start();
					playerrt = new ClientReading(s);
					Thread prt = new Thread(playerrt);
					prt.start();
					label_1.setText(MainWindow.hostName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
	}
	DatagramSocket ds = null;  //建立套间字udpsocket服务  
	//UDP发送房间
	class SendRoom implements Runnable{
		public void run() {
			while(true){
		        try {
		          ds = new DatagramSocket(8666);  //实例化套间字，指定自己的port  
		        } catch (SocketException e) {  
		            System.out.println("Cannot open port!");  
		            System.exit(1);   
		        }  
		        
		        String text = MainWindow.IPAdr + "@" + MainWindow.NAME + "@" + String.valueOf(playerNum) + "/4";
		        byte[] buf= text.getBytes();  //数据  
		        InetAddress destination = null ;
		        try {  
		            destination = InetAddress.getByName("255.255.255.255");  //广播
		        } catch (UnknownHostException e) {  
		            System.out.println("Cannot open findhost!");  
		            System.exit(1);   
		        }
		        DatagramPacket dp =   
		                new DatagramPacket(buf, buf.length, destination , 8086);    
		        //打包到DatagramPacket类型中（DatagramSocket的send()方法接受此类，注意8086是接受地址的端口，不同于自己的端口！）  
		           
		        try {  
		            ds.send(dp);  //发送数据  
		        } catch (IOException e) {  
		        }  
		        ds.close();  
		        
		        try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
	//创建空的服务器读与写线程
	ServerReading[] reading = new ServerReading[3];
	ServerWriting[] writing = new ServerWriting[3];
	ServerSocket ss = null;
	//创建TCP服务器
	class createTCP implements Runnable{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				ss = new ServerSocket(8686,3);			//TCP建立端口
				while(!ss.isClosed()){
					Socket s = ss.accept();
					if(playerNum!=0){
						playerNum++;
						playerIP[playerNum-2] = s.getInetAddress().toString().substring(1);
						//System.out.println("ha" + playerIP[playerNum-2]);
						//检测房间是否人满
						if(playerNum>4){
							s.close();
							playerNum--;
						}
						reading[playerNum-2] = new ServerReading(s);
						new Thread(reading[playerNum-2]).start();
						writing[playerNum-2] = new ServerWriting(s);
						new Thread(writing[playerNum-2]).start();
					}else{
						Thread.sleep(200);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//服务器读信息，采用TCP的方式
	class ServerReading implements Runnable{
		Socket socket = null;
		public ServerReading(Socket s) {
			this.socket = s;
		}
		public void run() {
			DataInputStream dis = null;
			try {
				while (true) {
					dis = new DataInputStream(socket.getInputStream());
					String[] state = dis.readUTF().split("@");					//state应该是一个长度为2的数组，0位为客户端名称，1位为客户端状态。
					//同步客户端名称信息
					if(playerNum!=0){
						playerName[playerNum-2] = state[0];
						
						labels[playerNum-2].setText(playerName[playerNum-2]);
						int ste = Integer.parseInt(state[1]);
						//System.out.println(ste);
						STATE[playerNum-2] = ste;
						switch (ste) {
							case 0:
							checkboxs[playerNum-2].setLabel("未准备");
							checkboxs[playerNum-2].setState(false);
							break;
							case 1:
							checkboxs[playerNum-2].setLabel("准备");
							checkboxs[playerNum-2].setState(true);
							default:
							break;
						}
					}
					//System.out.println(socket.isClosed());
					Thread.sleep(500);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(socket!=null && playerNum!=0){
					labels[playerNum-2].setText("等待加入");
					checkboxs[playerNum-2].setLabel("未准备");
					checkboxs[playerNum-2].setState(false);
					playerNum--;
					if(!socket.isClosed()){
						try {
							//System.out.println(playerNum);
							socket.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	//服务器写信息，采用TCP的方式
	class ServerWriting implements Runnable{
		
		Socket socket = null;
		
		public ServerWriting(Socket s) {
			socket = s;
		}

		public void run() {
			DataOutputStream dos = null;
			try {
				Thread.sleep(2000);
				while (true) {
					String msg = "";								//传输的msg的格式应该是客户端的姓名+状态，中间用@隔开。
					for(int i=0;i<=playerNum-2;i++){
						msg = msg + playerName[i] + "@";
						msg = msg + STATE[i] + "@";
					}
					msg = msg + START;
					dos = new DataOutputStream(socket.getOutputStream());
					dos.writeUTF(msg);
					dos.flush();
					Thread.sleep(500);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(dos!=null){
					try {
						dos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(socket!=null){
					if(!socket.isClosed()){
						try {
							socket.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	//客户端读信息，采用TCP的方式
	class ClientReading implements Runnable{
		
		Socket socket = null;
		
		public ClientReading(Socket s) {
			socket = s;
		}

		public void run() {
			DataInputStream dis = null;
			try {
				Thread.sleep(500);
				//System.out.println("haha");
				while (true) {
					dis = new DataInputStream(socket.getInputStream());
					String[] srvmsg = dis.readUTF().split("@");
					//System.out.println(srvmsg.length);
					//设置各个label的名称
					for(int i=0;i<srvmsg.length-1;i=i+2){
						labels[i/2].setText(srvmsg[i]);
						playerName[i/2] = srvmsg[i];
						//获取游戏人数
						playerNum = (srvmsg.length-1)/2 + 1;
						//判断如果名称是本客户端的名称，则开启准备按钮
						if(srvmsg[i].toString().equals(MainWindow.NAME)){
							SITE = i/2;
							GameWindow.SITE = SITE + 1;
							checkboxs[SITE].setEnabled(true);
						}
					}
					
					//设置各个checkbox的状态
					for(int i=1;i<srvmsg.length-1;i=i+2){
						//先回避服务器对本地的状态的改变冲突
						if(!srvmsg[i-1].toString().equals(MainWindow.NAME)){
							switch (Integer.parseInt(srvmsg[i])) {
							case 0:
								checkboxs[i-1/2].setLabel("未准备");
								checkboxs[i-1/2].setState(false);
								break;
							case 1:
								checkboxs[i-1/2].setLabel("准备");
								checkboxs[i-1/2].setState(true);
								break;
							default:
								break;
							}
						}
					}
					
					//判断是否已经开始游戏
					if(srvmsg[srvmsg.length-1].equals("1")){
						//System.out.println(srvmsg[srvmsg.length-1]);
						button.setEnabled(true);
						button.doClick();
						button.setEnabled(false);
					}
					
					Thread.sleep(500);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(socket!=null){
					if(!socket.isClosed()){
						try {
							socket.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	//客户端写信息，采用TCP的方式
	class ClientWriting implements Runnable{

		Socket socket = null;
		
		public ClientWriting(Socket s) {
			socket = s;
		}
		
		public void run() {
			DataOutputStream dos = null;
			try {
				while (true) {
					dos = new DataOutputStream(socket.getOutputStream());
					//写出自己当前的状态，0为未准备，1为准备
					String msg = MainWindow.NAME + "@" + ZT;
					dos.writeUTF(msg);
					dos.flush();
					Thread.sleep(500);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(dos!=null){
					try {
						dos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(socket!=null){
					if(!socket.isClosed()){
						try {
							socket.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	//建立一个线程，用于判断是否能开始游戏，以及限制只有房主才能开始游戏。
	boolean flag = true;			//设置一个flag用于控制线程结束。
	class GameStart implements Runnable{
		
		public void run() {
			if(sss[0].equals("0")){					//如果是服务器
				while (flag) {
					//System.out.println(checkboxs[0].isEnabled());
					int readyNum = 1;
					for(int i=0;i<3;i++){
						if(checkboxs[i].getState()){
							readyNum++;
						}
					}
					if(readyNum!=playerNum){
						button.setEnabled(false);
					}else{
						button.setEnabled(true);
					}
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	class CloseRoom implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			MainWindow.window.frmRunandsurvive.setVisible(false);
			button_1.doClick();
		}
		
	}
}
